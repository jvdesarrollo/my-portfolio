const files = {
  name: 'files',
  title: 'Files',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 96,
      },
    },
    {
      name: 'cv',
      title: 'CV',
      type: 'file',
      options: {
        hotspot: true,
      },
    },
  ],
};
export default files;
