const education = {
  name: 'education',
  title: 'Education',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Name, Institution or Instance',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 96,
      },
    },
    {
      name: 'init',
      title: 'Initiation',
      type: 'date',
      options: {
        dateFormat: 'DD-MM-YYYY',
      },
    },
    {
      name: 'finished',
      title: 'Finished',
      type: 'date',
      options: {
        dateFormat: 'DD-MM-YYYY',
      },
    },
    {
      name: 'titulation',
      title: 'Tituliation',
      type: 'string',
    },
    {
      name: 'state',
      title: 'State',
      type: 'string',
      options: {
        list: [
          { value: 'Terminado', title: 'Terminado' },
          { value: 'Cursando', title: 'En curso' },
          { value: 'Abandonado', title: 'Abandonado' },
        ],
      },
    },
  ],
};
export default education;
