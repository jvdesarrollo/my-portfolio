const project = {
  name: 'project',
  title: 'Project',
  type: 'document',
  fields: [
    {
      name: 'image',
      title: 'Image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'title',
      type: 'string',
    },
    // {
    //   name: 'date',
    //   type: 'datetime',
    // },
    // {
    //   name: 'place',
    //   type: 'string',
    // },
    {
      name: 'description',
      type: 'text',
    },
    {
      name: 'descripcion',
      type: 'text',
    },
    // {
    //   name: 'projectType',
    //   title: 'Project Type',
    //   type: 'string',
    //   options: {
    //     list: [
    //       { value: 'personal', title: 'Personal' },
    //       { value: 'client', title: 'Client' },
    //       { value: 'school', title: 'School' },
    //     ],
    //   },
    // },
    {
      name: 'link',
      type: 'url',
    },
    {
      name: 'detail',
      type: 'url',
    },

    {
      name: 'tags',
      type: 'array',
      of: [
        {
          type: 'string',
        },
      ],
      options: {
        layout: 'tags',
      },
    },
  ],
};
export default project;
