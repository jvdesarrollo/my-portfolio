import React, { useEffect, useState } from 'react';
import sanityClient from '../client';
import classes from './About.module.css';
import Spinner from './Spinner';
import { Trans } from 'react-i18next';
import Footer from '../components/Layouts/Footer/Footer';


const About = (props) => {
  const [files, setFiles] = useState(null);
  const [educationData, setEducation] = useState(null);
  const [skillsAndTools, setSkillsAndTools] = useState(null);

  useEffect(() => {
    sanityClient
      .fetch(
        `*[_type == "files"]{
      "cvURL": cv.asset->url
    }`
      )
      .then((data) => {
        setFiles(data[0]);
      })
      .catch(console.error);
    sanityClient
      .fetch(
        `*[_type == "education"]{
      name,
      init,
      finished,
      titulation,
      state
    }`
      )
      .then((data) => {
        setEducation(data);
      })
      .catch(console.error);
    sanityClient
      .fetch(
        `*[_type == "skillsAndTools"]{
      skills,
      tools
    }`
      )
      .then((data) => {
        setSkillsAndTools(data);
      })
      .catch(console.error);
  }, []);

  if (!files) return <Spinner />;
  if (!educationData) return <Spinner />;

  return (
    <>
    <main className={classes.Main}>
      <section className={classes.Container}>
        <div className={classes.Init}>
          <h1>Curriculum Vitae</h1>
          <p>
            <Trans i18nKey="about.pInit">
              Mi viaje como programador comenzó en 2019 como autodidacta. En
              estos años me he enfocado en el desarrollo frontend y en el diseño
              UI/UX. Actualmente comencé una tecnicatura superior en desarrollo
              web para profundizar mis conocimientos.{' '}
            </Trans>
          </p>
        </div>
        <div className={classes.Contain}>
          <div className={classes.Education}>
            <h3>
              <Trans i18nKey="about.h3.education">Educación</Trans>
            </h3>
            {educationData &&
              educationData.map((education, index) => (
                <div key={index}>
                  <hr />
                  <h4>{education.titulation}</h4>
                  <p className={classes.Name}>{education.name}</p>
                  <p className={classes.Date}>
                    {new Date(education.init).toLocaleDateString('es-AR')} -{' '}
                    {!education.finished
                      ? 'actualidad'
                      : new Date(education.finished).toLocaleDateString(
                          'es-AR'
                        )}
                  </p>
                  <h4> {education.state} </h4>
                </div>
              ))}
            <hr />
          </div>
          <div className={classes.Skills}>
            <h3>
              <Trans i18nKey="about.h3.tools">Habilidades y herramientas</Trans>
            </h3>
            <hr />
            <div className={classes.SkillsList}>
              <ul>
                {skillsAndTools &&
                  Object.values(skillsAndTools[0].skills).map(
                    (element, index) => <li key={index}> {element}</li>
                  )}
              </ul>
              <ul>
                {skillsAndTools &&
                  Object.values(skillsAndTools[0].tools).map(
                    (element, index) => <li key={index}> {element}</li>
                  )}
              </ul>
            </div>
            <hr />
          </div>
        </div>
        <div className={classes.Download}>
          <p>
            <Trans i18nKey="about.pDownload">Descarga mi CV en PDF.</Trans>
          </p>
          <a href={`${files.cvURL}?dl=Julio-Villar-CV.pdf`}>
            <img src="assets/icons/Download.svg" alt="icon" />
            <Trans i18nKey="about.button">DESCARGAR</Trans>
          </a>
        </div>
      </section>
      </main>
      <Footer/>
      </>
  );
};

export default About;
