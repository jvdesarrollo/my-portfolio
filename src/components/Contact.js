import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import paper from '../paper.svg';
import classes from './Contact.module.css';
// import Spinner from './Spinner';
import axios from 'axios';
import Aux from '../hoc/Auxiliary/Auxiliary';
import { Trans } from 'react-i18next';
import Footer from '../components/Layouts/Footer/Footer';


const Contact = (props) => {
  const [name, setName] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');

  const post = {
    FromName: 'Mailing Service',
    FromAddress: 'consultas@draftmailingservice.com.ar',
    To: ['jvdesarrollo@gmail.com'],
    Subject: 'Consulta',
    Body: `<div>
    <strong>Nombre: ${name}</strong>
    Asunto: ${subject}
    Mensaje: ${message}
    </div>
    `,
  };
  const history = useHistory();

  const submit = (e) => {
    e.preventDefault();
    axios
      .post('https://draftmailingservice.com.ar/api/mailing', post)
      .then(function (response) {
        history.push('/Success');
      })
      .catch((error) => {
        console.log(error);
        history.push('/Error');
      });
  };

  return (
    <Aux>
      {/* {isLoading ? <form style={{ opacity: 0.3 }}></form> : null} */}
      <div className={classes.FormContainer}>
        <div className={classes.FormHeader}>
          <h1>
            <Trans i18nKey="contact.h1">Contacto</Trans>
          </h1>
          <p>
            <Trans i18nKey="contact.p">
              Contactame si te interesan mis servicios o colaboración,
              contestaré a la brevedad.
            </Trans>
          </p>
        </div>
        <div className={classes.FormGrid}>
          <div className={classes.Container}>
            <form onSubmit={submit}>
              <div className={classes.Contain}>
                <label htmlFor="nombre">
                  <Trans i18nKey="contact.name">Nombre</Trans>
                </label>
                <input
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                <label htmlFor="asunto">
                  <Trans i18nKey="contact.subject">Asunto</Trans>
                </label>
                <input
                  type="text"
                  value={subject}
                  onChange={(e) => setSubject(e.target.value)}
                />
                <label htmlFor="mensaje">
                  <Trans i18nKey="contact.message">Mensaje</Trans>
                </label>
                <textarea
                  name="mensaje"
                  id="message"
                  cols="30"
                  rows="10"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                ></textarea>
                <button
                  className={classes.Button}
                  type="submit"
                  onClick={(e) => {}}
                >
                  <img src={paper} alt="icon" />
                  <Trans i18nKey="contact.button">Enviar</Trans>
                </button>
              </div>
            </form>
          </div>
          <div className={classes.FormFooter}>
            <h3>
              <Trans i18nKey="contact.h3">Hablemos</Trans>
            </h3>
            <ul>
              <li>
                <img src="assets/icons/envelope.svg" alt="icon" />
                jvdesarrollo@gmail.com
              </li>
              <li>
                <img src="assets/icons/whatsapp.svg" alt="icon" />
                +5493516805029
              </li>
            </ul>
          </div>
        </div>
      </div>
      <Footer/>
    </Aux>
  );
};

export default Contact;
