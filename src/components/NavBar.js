import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './NavBar.module.css';
import SideBar from '../components/SideBar';
import { useTranslation } from 'react-i18next';
import { Trans } from 'react-i18next';

const NavBar = () => {
  const { i18n } = useTranslation();

  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  };
  return (
    <header className={classes.Header}>
      <div className={classes.Logo}>
        {' '}
        <NavLink to="/" exact>
          JV
        </NavLink>{' '}
      </div>
      <div className={classes.HeaderElements}>
        <nav className={classes.Navbar}>
          <NavLink to="/project" activeClassName={classes.active} className="">
            <Trans i18nKey="navbar.projects">Proyectos</Trans>
          </NavLink>
          <NavLink to="/about" activeClassName={classes.active} className="">
            Curriculum Vitae
          </NavLink>
          <NavLink to="/contact" activeClassName={classes.active} className="">
            <Trans i18nKey="navbar.contact">Contacto</Trans>
          </NavLink>
        </nav>
      </div>
      <div className={classes.Lang}>
        <ul>
          <li>
            <a href="#" onClick={() => changeLanguage('es')}>
              SPA
            </a>
          </li>
          <li>
            {' '}
            <img src="assets/img/line.svg" alt="line" />{' '}
          </li>
          <li>
            <a href="#" onClick={() => changeLanguage('en')}>
              ENG
            </a>
          </li>
        </ul>
      </div>
      <div className={classes.SocialIcon}>
        <ul>
          <li>
            <a
              href="https://www.linkedin.com/in/julio-villar-c%C3%B3rdoba-arg-126601200/"
              target="_blank"
              rel="noreferrer"
            >
              <img src="assets/icons/linkedin.svg" alt="icon" />
            </a>
          </li>
          <li>
            <a
              href="https://gitlab.com/jvdesarrollo"
              target="_blank"
              rel="noreferrer"
            >
              <img src="assets/icons/gitlab.svg" alt="icon" />
            </a>
          </li>
          <li>
            <a
              href="https://wa.me/+5493516805029?"
              target="_blank"
              rel="noreferrer"
            >
              <img src="assets/icons/whatsapp.svg" alt="icon" />
            </a>
          </li>
        </ul>
      </div>
      <div className={classes.MobileMenu}>
        <SideBar></SideBar>
      </div>
    </header>
  );
};

export default NavBar;
