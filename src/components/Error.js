import React from 'react';
import Error from '../Error-Icon.svg';
import classes from './Error.module.css';

const withError = () => {
  return (
    <div className={classes.Container}>
      <div>
        <img src={Error} alt="icon" />
      </div>
      <div>
        <p>
         Algo salió mal, vuelve a intentar más tarde.
        </p>
      </div>
    </div>
  );
};

export default withError;
