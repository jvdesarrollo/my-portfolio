import React from 'react';
import classes from './primary.module.css';

/**
 * @author
 * @function PrimaryButton
 **/

const PrimaryButton = (props) => {
  return (
    <div>
      <button className={classes.Button}>{props.children}</button>
    </div>
  );
};

export default PrimaryButton;
