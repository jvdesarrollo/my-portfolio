import React from 'react';
import classes from './secondary.module.css';

/**
 * @author
 * @function SecondatyButton
 **/

const SecondatyButton = (props) => {
  return (
    <div>
      <button className={classes.Button}>{props.children}</button>
    </div>
  );
};

export default SecondatyButton;
