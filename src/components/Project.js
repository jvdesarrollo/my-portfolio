import React, { useEffect, useState } from 'react';
import sanityClient from '../client';
import classes from './Project.module.css';
import Spinner from './Spinner';
import imageUrlBuilder from '@sanity/image-url';
import PrimaryButton from './UI/PrimaryButton/primary';
import SecondatyButton from './UI/SecondaryButton/socondary';
import Footer from '../components/Layouts/Footer/Footer';
import { Trans, useTranslation } from 'react-i18next';

const builder = imageUrlBuilder(sanityClient);
function urlFor(source) {
  return builder.image(source);
}

const Project = () => {
  const [projectData, setProjectData] = useState(null);

  const { i18n } = useTranslation('es');

  useEffect(() => {
    sanityClient
      .fetch(
        `*[_type == "project"]{
      title,
      image,
      date,
      place,
      description,
      descripcion,
      projectType,
      detail,
      link,
      tags
    }`
      )
      .then((data) => {
        setProjectData(data);
      })
      .catch(console.error);
  }, []);

  if (!projectData) return <Spinner />;

  return (
    <>
      <main className={classes.Main}>
        <section className={classes.Section}>
          <h1>
            <Trans i18nKey="projects.h1">Mis Proyectos</Trans>
          </h1>
          <div className={classes.CardHorizontal}>
            <section className={classes.CardContainer}>
              {projectData &&
                projectData.map((project, index) => (
                  <article className={classes.Card} key={index}>
                    <div className={classes.CardImg}>
                      <img src={urlFor(project.image)} alt="img" />
                    </div>
                    <div className={classes.CardContain}>
                      <h3 className="">
                        <a
                          href={project.link}
                          alt={project.title}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {project.title}
                        </a>
                      </h3>
                      <p>{project.description}</p>
                      <div className={classes.CardButtons}>
                        <a
                          href={project.link}
                          rel="noopener noreferrer"
                          target="_blank"
                        >
                          <PrimaryButton>
                            <img
                              src="assets/icons/eye-regular.svg"
                              alt="icon"
                            />
                            <Trans i18nKey="projects.details">
                              VER DETALLES
                            </Trans>
                          </PrimaryButton>
                        </a>
                        <a
                          href={project.detail}
                          rel="noopener noreferrer"
                          target="_blank"
                        >
                          <SecondatyButton>
                            <img src="assets/icons/small-git.svg" alt="icon" />
                            <Trans i18nKey="projects.repository">
                              REPOSITORIO
                            </Trans>
                          </SecondatyButton>
                        </a>
                      </div>
                    </div>
                  </article>
                ))}
            </section>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default Project;
