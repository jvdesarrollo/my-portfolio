import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import classes from './SideBar.module.css';
import Aux from '../hoc/Auxiliary/Auxiliary';

const SideBar = (props) => {
  const [sidebar, setSidebar] = useState(false);
  const showSidebar = () => {
    setSidebar(!sidebar);
  };
  const [active, setActive] = useState(false);
  const setActiveClass = () => {
    setActive(!active);
  };

  return (
    <Aux>
      <div
        onClick={() => {
          showSidebar();
          setActiveClass();
        }}
        className={sidebar ? `${classes.Backdrop}` : ``}
      ></div>
      <div
        onClick={showSidebar}
        className={
          active ? `${classes.Burgers} ${classes.active}` : `${classes.Burgers}`
        }
      >
        <div className={classes.Menu} onClick={setActiveClass}>
          MENU
        </div>
        <button
          type="button"
          className={classes.Burgerbutton}
          title="Menu"
          onClick={setActiveClass}
        >
          <span className={`${classes.Burgerbar} ${classes.Burgerbar1}`}></span>
          <span className={`${classes.Burgerbar} ${classes.Burgerbar2}`}></span>
          <span className={`${classes.Burgerbar} ${classes.Burgerbar3}`}></span>
        </button>
      </div>
      <div
        className={
          sidebar
            ? `${classes.SideBar} ${classes.Open}`
            : `${classes.SideBar} ${classes.Close}`
        }
      >
        <nav
          onClick={() => {
            showSidebar();
            setActiveClass();
          }}
          className={classes.Navbar}
        >
          <NavLink to="/" exact activeClassName={classes.active}>
            Inicio
          </NavLink>
          <NavLink to="/project" activeClassName={classes.active} className="">
            Proyectos
          </NavLink>
          <NavLink to="/about" activeClassName={classes.active} className="">
            Curriculum Vitae
          </NavLink>
          <NavLink to="/contact" activeClassName={classes.active} className="">
            Contacto
          </NavLink>
        </nav>
        <div className={classes.SocialIcon}>
          <ul>
            <li>
              <a href="https://www.linkedin.com/in/julio-villar-c%C3%B3rdoba-arg-126601200/">
                <img src="assets/icons/linkedin.svg" alt="icon" />
              </a>
            </li>
            <li>
              <a href="https://gitlab.com/jvdesarrollo">
                <img src="assets/icons/gitlab.svg" alt="icon" />
              </a>
            </li>
            <li>
              <a href="https://wa.me/+5493516805029?">
                <img src="assets/icons/whatsapp.svg" alt="icon" />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </Aux>
  );
};

export default SideBar;
