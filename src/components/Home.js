import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './Home.module.css';
import PrimaryButton from './UI/PrimaryButton/primary';
import SecondatyButton from './UI/SecondaryButton/socondary';
import { Trans } from 'react-i18next';
import Footer from '../components/Layouts/Footer/Footer';

const Home = () => {
  return (
    <>
      <main>
        <section className={classes.Home}>
          <img src="assets/img/Julio.png" alt="img" />
          <div className={classes.HomeContent}>
            <h1>
              <Trans i18nKey="home.h1">Hola, mi nombre es Julio, y soy</Trans>
              <span>
                {' '}
                <Trans i18nKey="home.span">
                  desarrollador frontend Junior.
                </Trans>{' '}
              </span>
            </h1>
            <p>
              <Trans i18nKey="home.p">
                Soy un desarrollador frontend Junior y diseñador UI Junior de
                Córdoba Argentina. Tengo un año de experiencia como freelance
                habiendo realizado proyectos para empresas locales. Estoy en
                búsqueda de nuevas oportunidades y proyectos.
              </Trans>
            </p>
            <div className={classes.HomeButtons}>
              <NavLink to="/contact">
                <PrimaryButton>
                  <Trans i18nKey="home.btn-primary">Contacto</Trans>
                </PrimaryButton>
              </NavLink>
              <NavLink to="/project">
                <SecondatyButton>
                  <Trans i18nKey="home.btn-secondary">proyectos</Trans>
                </SecondatyButton>
              </NavLink>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
};

export default Home;
