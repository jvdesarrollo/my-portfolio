import React from 'react';
import classes from './Footer.module.css';
import { Trans } from 'react-i18next';

/**
 * @author
 * @function Footer
 **/

const Footer = () => {
  return (
    <div className={classes.Footer}>
      <footer>
        <Trans i18nKey="footer">
          Diseñado y desarrolaldo por Julio Villar
        </Trans>
      </footer>
    </div>
  );
};

export default Footer;
