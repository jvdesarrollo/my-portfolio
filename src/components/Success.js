import React from 'react';
import success from '../Success-Icon.svg';
import classes from './Success.module.css';

const Success = () => {
  return (
    <div className={classes.Container}>
      <div>
        <img src={success} alt="icon" />
      </div>
      <div>
        <p>
          Gracias por enviar tu consulta, me pondré en contacto a la brevedad.
        </p>
      </div>
    </div>
  );
};

export default Success;
