import sanityClient from '@sanity/client';

export default sanityClient({
  projectId: 'vhp730sb',
  dataset: 'production',
});
