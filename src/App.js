import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import Project from './components/Project';
import NavBar from './components/NavBar';
import Footer from './components/Layouts/Footer/Footer';
import Contact from './components/Contact';
import Success from './components/Success';
import Error from './components/Error';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route component={Home} path="/" exact />
        <Route component={About} path="/about" />
        <Route component={Contact} path="/contact" />
        <Route component={Project} path="/project" />
        <Route component={Success} path="/success" />
        <Route component={Error} path="/error" />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
